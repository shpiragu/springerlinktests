Feature: Search

  Background: Link Springer Site
    Given user is on the home page

  Scenario: Filter by access and relevance
    When user searches for "DNA"
    And user does not include restricted content
    And user sorts by "relevance"
    And user sorts by documents published between 2015 and 2020
    Then articles should have been published between 2015 and 2020
    And articles should contain
      | DNA    |

  Scenario: Filter by language and newest first
    When user searches for "algebra"
    And user does include restricted content
    And user sorts by "newest first"
    And user sorts by documents published in 2010
    And user selects documents published in "English"
    Then articles should have been published in 2010
    And articles should contain
      | algebra |