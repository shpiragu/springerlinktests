package com.springer.link.home;

import com.springer.link.util.Browser;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;

public class HomepageSteps {

    @Given("^user is on the home page$")
    public void user_is_on_the_home_page() throws Throwable {
        Browser.getDriver().get("https://link.springer.com/");
    }

}
