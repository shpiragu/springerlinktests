package com.springer.link.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public final class Browser {

    private static WebDriver driver;

    protected static void startUp() {
        driver = new ChromeDriver();
    }

    protected static void shutDown() {
        if(driver != null) {
            driver.quit();
        }
    }

    public static WebDriver getDriver() {
        return driver;
    }

}
