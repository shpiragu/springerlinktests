package com.springer.link.util;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

    @Before
    public void beforeScenario() {
        Browser.startUp();
    }

    @After
    public void afterScenario() {
        Browser.shutDown();
    }

}
