package com.springer.link.util;

public enum DocumentSort {

    RELEVANCE, NEWEST_FIRST, OLDEST_FIRST

}
