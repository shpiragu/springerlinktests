package com.springer.link.search;

import com.springer.link.util.Browser;
import com.springer.link.util.DocumentSort;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

import java.util.List;

public class SearchSteps {

    private void expandFunctionsBar() {
        WebElement button = Browser.getDriver().findElement(By.className("expander-title"));

        boolean expanded = Boolean.parseBoolean(button.getAttribute("aria-expanded"));

        if(!expanded) {
            button.click();
        }
    }

    @When("^user searches for \"([^\"]*)\"$")
    public void user_searches_for(String search) throws Throwable {
        Browser.getDriver().findElement(By.id("query")).sendKeys(search);

        Browser.getDriver().findElement(By.id("search")).click();
    }

    @And("^user sorts by documents published between (\\d+) and (\\d+)$")
    public void user_sorts_by_documents_published_between_and(int startYear, int endYear) throws Throwable {
        expandFunctionsBar();

        (new WebDriverWait(Browser.getDriver(), 10)).until(ExpectedConditions.elementToBeClickable(By.id("date-facet-submit")));

        new Select(Browser.getDriver().findElement(By.id("date-facet-mode"))).selectByVisibleText("between");

        WebElement startYearElement = Browser.getDriver().findElement(By.id("start-year"));
        WebElement endYearElement = Browser.getDriver().findElement(By.id("end-year"));

        startYearElement.clear();
        startYearElement.sendKeys(String.valueOf(startYear));

        endYearElement.clear();
        endYearElement.sendKeys(String.valueOf(endYear));

        Browser.getDriver().findElement(By.id("date-facet-submit")).click();
    }

    @And("^user (\\w+( \\w+)?) include restricted content$")
    public void user_does_include_restricted_content(String option) throws Throwable {
        boolean enable = option == null;

        WebElement checkBox = Browser.getDriver().findElement(By.id("results-only-access-checkbox"));

        if(enable != checkBox.isSelected()) {
            checkBox.click();
        }
    }

    @Then("^articles should contain$")
    public void articles_should_contain(DataTable termsTable) throws Throwable {
        List<String> terms = termsTable.column(0);

        List<WebElement> articles = Browser.getDriver().findElements(By.cssSelector("#results-list li"));

        for(WebElement a : articles) {
            String type = a.findElement(By.className("content-type")).getText().trim();

            if(!type.equalsIgnoreCase("article") && !type.toLowerCase().contains("chapter")) {
                continue;
            }

            String title = a.findElement(By.cssSelector("h2")).getText();
            String snippet = a.findElement(By.className("snippet")).getText();
            String meta = "";

            WebElement metaElement = a.findElement(By.className("meta"));

            if(metaElement.isDisplayed()) {
                meta = metaElement.getText();
            }

            for(String t : terms) {
                if(!(title.toLowerCase().contains(t.toLowerCase()) || snippet.toLowerCase().contains(t.toLowerCase()) || meta.toLowerCase().contains(t.toLowerCase()))) {
                    System.err.println(title);
                    System.err.println(snippet);
                    System.err.println(meta);
                }

                assertTrue("Keyword missing from article with respect to the search performed!", title.toLowerCase().contains(t.toLowerCase()) || snippet.toLowerCase().contains(t.toLowerCase()) || meta.toLowerCase().contains(t.toLowerCase()));
            }
        }
    }

    @And("^articles should have been published between (\\d+) and (\\d+)$")
    public void articles_should_have_been_published_between_and(int startYear, int endYear) throws Throwable {
        List<WebElement> articles = Browser.getDriver().findElements(By.cssSelector("#results-list li"));

        for(WebElement a : articles) {
            String type = a.findElement(By.className("content-type")).getText().trim();

            if(!type.equalsIgnoreCase("article") && !type.toLowerCase().contains("chapter")) {
                continue;
            }

            String yearStr = a.findElement(By.cssSelector(".meta .enumeration .year")).getText();

            int year = Integer.parseInt(yearStr.substring(1, yearStr.length() - 1));

            assertTrue("Article not published within the time period specified!", year >= startYear && year <= endYear);
        }
    }

    @And("^user sorts by \"([^\"]*)\"$")
    public void user_sorts_by(String sort) throws Throwable {
        List<WebElement> sortElements = Browser.getDriver().findElements(By.cssSelector("#sort-results .btn"));

        DocumentSort documentSort = DocumentSort.valueOf(sort.replaceAll(" ", "_").toUpperCase());

        sortElements.get(documentSort.ordinal()).click();
    }

    @And("^user sorts by documents published in (\\d+)$")
    public void user_sorts_by_documents_published_in(int year) throws Throwable {
        expandFunctionsBar();

        (new WebDriverWait(Browser.getDriver(), 10)).until(ExpectedConditions.elementToBeClickable(By.id("date-facet-submit")));

        new Select(Browser.getDriver().findElement(By.id("date-facet-mode"))).selectByVisibleText("in");

        WebElement startYearElement = Browser.getDriver().findElement(By.id("start-year"));

        startYearElement.clear();
        startYearElement.sendKeys(String.valueOf(year));

        Browser.getDriver().findElement(By.id("date-facet-submit")).click();
    }

    @And("^user selects documents published in \"([^\"]*)\"$")
    public void user_selects_documents_published_in(String lang) throws Throwable {
        List<WebElement> languages = Browser.getDriver().findElements(By.cssSelector("#language-facet ol li"));

        for(WebElement l : languages) {
            WebElement link = l.findElement(By.cssSelector("a"));

            if(link.findElement(By.className("facet-title")).getText().trim().equalsIgnoreCase(lang.trim())) {
                link.click();

                break;
            }
        }
    }

    @Then("^articles should have been published in (\\d+)$")
    public void articles_should_have_been_published_in(int year) throws Throwable {
        List<WebElement> articles = Browser.getDriver().findElements(By.cssSelector("#results-list li"));

        for(WebElement a : articles) {
            String type = a.findElement(By.className("content-type")).getText().trim();

            if(!type.equalsIgnoreCase("article") && !type.toLowerCase().contains("chapter")) {
                continue;
            }

            String yearStr = a.findElement(By.cssSelector(".meta .enumeration .year")).getText();

            int y = Integer.parseInt(yearStr.substring(1, yearStr.length() - 1));

            assertTrue("Article not published within the time period specified!", y == year);
        }
    }

}
